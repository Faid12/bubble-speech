# Bubble Speech

Bubble Speech
Faidurrohman - 4210181013
Desain Multiplayer Game Online

## gif
![](4210181013_Faidurrohman.gif)

## Code

Deskripsi Code


    public class TheText : MonoBehaviour
    {
        // deklarsi text
        public Text thetext;

        // untuk memanggil web
        private string url = "https://5e510330f2c0d300147c034c.mockapi.io/users";

        public void GetJsonData()
        {
            // sebagai delay
            StartCoroutine(ProcessGetJson());
        }
        private IEnumerator ProcessGetJson()
        {
            // memanggil file json di web menggunakan UntuyWebRequest
            UnityWebRequest www = UnityWebRequest.Get(url);
            yield return www.SendWebRequest();
    
            // mengubah file json yang dipanggil menjadi text
            TheData data = getData(www.downloadHandler.text);

            // menanmpilkan text ketika tombol ditekan
            thetext.text = $"My name is {data.name}.\nMy email is \n{data.email}.";
        
        }
        TheData getData(string json)
        {
            // memanggil array ke 13 dari web
            JSONArray array = JSON.Parse(json).AsArray;
            return new TheData(array[13]["name"].Value, array[13]["email"]);
        }
    }
    public class TheData
    {
        public string name;
        public string email;

        public TheData(string name, string email)
        {
            this.name = name;
            this.email = email;
        }
    }




